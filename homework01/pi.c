#include <stdlib.h> 
#include <stdio.h>
#include <math.h>
#include <omp.h>
#include "common.h"
#include <inttypes.h>
#include <time.h>


#define PI 3.1415926535


void usage(int argc, char** argv);
double calcPi_Serial(int num_steps);
double calcPi_P1(int num_steps);
double calcPi_P2(int num_steps);
double calcPi_Monte_Carlo(int num_steps);


int main(int argc, char** argv)
{
    // get input values
    uint32_t num_steps = 100000;
    if(argc > 1) {
        num_steps = atoi(argv[1]);
    } else {
        usage(argc, argv);
        printf("using %"PRIu32"\n", num_steps);
    }
    fprintf(stdout, "The first 10 digits of Pi are %0.10f\n", PI);


    // set up timer
    uint64_t start_t;
    uint64_t end_t;
    InitTSC();


    // calculate in serial 
    start_t = ReadTSC();
    double Pi0 = calcPi_Serial(num_steps);
    end_t = ReadTSC();
    printf("Time to calculate Pi serially with %"PRIu32" steps is: %g\n",
        num_steps, ElapsedTime(end_t - start_t));
    printf("Pi is %0.10f\n", Pi0);
    

    // calculate in parallel with reduce 
    start_t = ReadTSC();
    double Pi1 = calcPi_P1(num_steps);
    end_t = ReadTSC();
    printf("Time to calculate Pi in // with %"PRIu32" steps is: %g\n",
           num_steps, ElapsedTime(end_t - start_t));
    printf("Pi is %0.10f\n", Pi1);


    // calculate in parallel with atomic add
    start_t = ReadTSC();
    double Pi2 = calcPi_P2(num_steps);
    end_t = ReadTSC();
    printf("Time to calculate Pi in // + atomic with %"PRIu32" steps is: %g\n",
           num_steps, ElapsedTime(end_t - start_t));
    printf("Pi is %0.10f\n", Pi2);

    
    // calculate in serial (using monte-carlo)
    start_t = ReadTSC();
    double Pi4 = calcPi_Monte_Carlo(num_steps);
    end_t = ReadTSC();
    printf("Time to calculate Pi using Monte Carlo (serially) with %"PRIu32" steps is: %g\n",
        num_steps, ElapsedTime(end_t - start_t));
    printf("Pi is %0.10f\n", Pi4);

    return 0;
}


void usage(int argc, char** argv)
{
    fprintf(stdout, "usage: %s <# steps>\n", argv[0]);
}

double calcPi_Serial(int num_steps)
{   // Declare variables
    double pi = 0.0;
    double step;
    double stepSize = 1.0 / num_steps;
    double y = 0;

    // Iterate over each "rectangle"
    for (step = stepSize; step < 1; step += stepSize) {
        // Determine the height of the rectangle
        y = sqrt(1 - pow(step, 2));
        // Add the rectangle in
        pi += stepSize * y;
    }
    // We only went over the top right quadrant, so using symmetry
    // include the other three quadrants
    pi *= 4;

    return pi;
}

double calcPi_P1(int num_steps)
{
    // Declare variables
    double pi = 0.0;
    double stepSize = 1.0 / num_steps;
    int i;

    // Reduce over pi
    #pragma omp parallel for private(i) reduction(+:pi)
    for (i = 0; i < num_steps; i++) {
        // Calculate this rectangle for all quadrants
        double y = sqrt(1 - pow(i * stepSize, 2));
        pi += 4 * stepSize * y;
    }
    return pi;
}

double calcPi_P2(int num_steps)
{
    // Declare variables
    double pi = 0.0;
    double stepSize = 1.0 / num_steps;
    int i;

    // iterate with lots of threads
    #pragma omp parallel for private(i)
    for (i = 0; i < num_steps; i++) {
        // Calculate this rectangle for all quadrants
        double y = sqrt(1 - pow(i * stepSize, 2));
        // Add to pi atomically
        #pragma omp atomic
        pi += 4 * stepSize * y;
    }
    return pi;
}

double calcPi_Monte_Carlo(int num_steps)
{   // Declare variables
    double xCord = 0.0;
    double yCord = 0.0;
    int inside = 0;
    int i = 0;

    // Throw a "dart" for each step.
    for (i = 0; i < num_steps; i++) {
        // Generate the dart throw coordinates
        // NOTE: This is not fully precise due to the nature of rand
        // returning an int.
        xCord = ((double)rand() * (1 - (-1))) / (double)RAND_MAX + (-1);
        yCord = ((double)rand() * (1 - (-1))) / (double)RAND_MAX + (-1);
        // Check if the point is inside of (or on) the circle:
        if (pow(xCord,2) + pow(yCord,2) <= 1) {
            //printf("(%f, %f) is outside the circle \n", xCord, yCord);
            inside++;
        }
    }
    // using the ratio, calculate pi
    return 4 * inside/(double)(num_steps);
}
